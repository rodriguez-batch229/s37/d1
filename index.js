const express = require("express");
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const cors = require("cors");
const userRoutes = require("./routes/user");
const courseRoutes = require("./routes/course");

const app = express();

// Mongodb Connection
mongoose.connect(
    "mongodb+srv://admin:admin1234@cluster0.tt3qfyy.mongodb.net/s37-s41?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }
);

// SET Notification for connection success or failure
let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.on("open", () => console.log("We are connected to cloud database"));

// Middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${process.env.PORT}`);
});
