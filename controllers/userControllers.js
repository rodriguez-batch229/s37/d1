const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const jwt = require("jsonwebtoken");
const Course = require("../models/Course.js");

// Check if the email already exists
/*
    Steps: 
    1. Use mongoose "find" method to find duplicate emails
    2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
*/

module.exports.checkEmailExists = (reqBody) => {
    return User.find({ email: reqBody.email }).then((result) => {
        if (result.length > 0) {
            return true;
        } else {
            return false;
        }
    });
};

// User registration
/*
			Steps:
			1. Create a new User object using the mongoose model and the information from the request body
			2. Make sure that the password is encrypted
			3. Save the new User to the database
		*/
module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        //  10 is the value provided as the number of salt
        password: bcrypt.hashSync(reqBody.password, 10),
    });

    // Saves the created object to our database
    return newUser.save().then((user, error) => {
        // registration failed
        if (error) {
            return false;
        } else {
            // registration successful
            return true;
        }
    });
};

// User authentication
/*
    Steps:
    1. Check the database if the user email exists
    2. Compare the password provided in the login form with the password stored in the database
    3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/
module.exports.loginUser = (reqBody) => {
    return User.findOne({ email: reqBody.email }).then((result) => {
        if (result == null) {
            return false;
        } else {
            const isPasswordCorrect = bcrypt.compareSync(
                reqBody.password,
                result.password
            );
            if (isPasswordCorrect) {
                return { access: auth.createAccessToken(result) };
            }
        }
    });
};

// Get Profile
/* 
    Steps:
    1. find the document in the database using the User's ID
    2. Reassign the password to the returned document to an empty string
    3. return the result back to the frontend

*/

module.exports.getProfile = (data) => {
    return User.findById(data.userId).then((result) => {
        // return password hidden
        result.password = "";
        console.log(result);

        return result;
    });
};

module.exports.allUsers = (result, err) => {
    return User.find(result).then((result) => {
        return result;
    });
};

module.exports.allAdmin = (result, err) => {
    return User.find({ isAdmin: true }).then((result) => {
        return result.name;
    });
};

// Enrollment

/* 
    Steps:
    1. Find the document in the database using the user's id
    2. Add the course id to the user's enrollment array
    3. Update the document
*/
// Async wait - will be used in enrolling the user
// This will need to update 2 seperate documents
module.exports.enroll = async (data) => {
    // Creates isUserUpdated variable that wil return true upon successful update
    // await keyword - will allow the enroll method to complete updating the user before returning a response back to the frontend
    let isUserUpdated = await User.findById(data.userId).then((user) => {
        // add the courseId to the user's enrollment array
        user.enrollments.push({ courseId: data.courseId });
        // saves the updated user information in the database
        return user.save().then((user, error) => {
            // If failed
            if (error) {
                return false;
            }
            // if success
            else {
                return true;
            }
        });
    });
    // adds the user id in the enrollees array of the course
    // await - to allow the enroll method to complete updating the course before returning a response to the frontend
    let isCourseUpdated = await Course.findById(data.courseId).then(
        (course) => {
            // add the user id in the course's enrollees array
            course.enrollees.push({ userId: data.userId });

            // Saves the updated course info in the database
            return course.save().then((course, error) => {
                if (error) {
                    return false;
                } else {
                    return true;
                }
            });
        }
    );

    if (isUserUpdated && isCourseUpdated) {
        return true;
    } else {
        return false;
    }
};
