const Course = require("../models/Course");
const User = require("../models/User");
const auth = require("../auth");
const userControllers = require("../controllers/userControllers.js");

/* 
    Steps:
    1. Create a new Course object using the mongoose model
    2. Save the new Course to the database
*/

module.exports.addCourse = (reqBody) => {
    //  Create a new variable "newCourse" and instantiate a new Course object
    // Uses the info from the request body  to provide all necessary information

    let newCourse = new Course({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
    });
    //  Saves the created object
    return newCourse.save().then((course, error) => {
        //  course creation failed
        if (error) {
            return false;
        }
        // Course created successfully
        else {
            return "Sucessfully created";
        }
    });
};

// get all courses
module.exports.getAllCourses = (result, error) => {
    return Course.find({}).then((results) => {
        return results;
    });
};

/* 
    Mini Activity
    1. Create a route that will retrieve all active courses
    2. no need user for login
    3. Create a controller that will return all active courses
*/

module.exports.getAllActiveCourses = (result, error) => {
    return Course.find({ isActive: true }).then((results) => {
        if (results) {
            return results;
        } else {
            return "There is no available";
        }
    });
};

// retrieve specific course
module.exports.getCourse = (reqParams) => {
    console.log(reqParams);
    return Course.findById(reqParams.courseId).then((result) => {
        console.log(result);
        return result;
    });
};

// update a course
/* 
    Steps:
    1. Create a variable "updatedCourse" which will contain the info retrieved from the req body
    2. Find and update the course using the course ID retrieved from the req params and the variable "updatedCourse" 
*/

// info for updating will be coming from url parameters and request body
module.exports.updateCourse = (reqParams, reqBody) => {
    // specify the fields of the document to be updated
    let updatedCourse = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
    };

    // findByIdandUpdate (document ID, updatesToBeApplied)
    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then(
        (course, error) => {
            // Course not updated
            if (error) {
                return false;
            }
            // Course updated
            else {
                return "Sucessfully updated";
            }
        }
    );
};

// module.exports.archiveCourse = (reqParams, reqBody) => {
//     let archiveCourse = new Course({
//         isActive: reqBody.isActive,
//     });

//     return Course.findByIdAndUpdate(reqParams.courseId, archiveCourse).then(
//         (course, error) => {
//             // Course Archived not success
//             if (error) {
//                 return false;
//             }
//             // Course Archived Success
//             else {
//                 return "Archive Sucessfully";
//             }
//         }
//     );
// };
module.exports.archiveCourse = (reqParams, reqBody) => {
    // specify the fields of the document to be updated
    let archiveCourse = {
        isActive: reqBody.isActive,
    };

    // findByIdandUpdate (document ID, updatesToBeApplied)
    return Course.findByIdAndUpdate(reqParams.courseId, archiveCourse).then(
        (course, error) => {
            // Course not updated
            if (error) {
                return false;
            }
            // Course updated
            else {
                return "Archived Successfully";
            }
        }
    );
};
