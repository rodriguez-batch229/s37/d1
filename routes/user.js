const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth");
const jwt = require("jsonwebtoken");

// Check email if it existing to our database

router.post("/checkEmail", (req, res) => {
    userControllers.checkEmailExists(req.body).then((resultFromController) => {
        res.send(resultFromController);
    });
});

// User registration route
router.post("/register", (req, res) => {
    userControllers.registerUser(req.body).then((resultFromController) => {
        res.send(resultFromController);
    });
});

// User Login
router.post("/login", (req, res) => {
    userControllers.loginUser(req.body).then((resultFromController) => {
        res.send(resultFromController);
    });
});

// Retrieving user details
// auth.verify - middleware to ensure that the user is logged in before they can retrieve their details
router.post("/details", auth.verify, (req, res) => {
    // uses the decode method defined in the auth.js to retrieve user info from the request header
    const userData = auth.decode(req.headers.authorization);

    userControllers
        .getProfile({ userId: req.body.id })
        .then((resultFromController) => {
            res.send(resultFromController);
        });
});
// Get all Users
router.get("/allUsers", (req, res) => {
    userControllers.allUsers(req.body).then((resultFromController) => {
        res.send(resultFromController);
    });
});

router.get("/adminUser", (req, res) => {
    userControllers.allAdmin(req.body).then((resultFromController) => {
        res.send(resultFromController);
    });
});

// Enroll a user

router.post("/enroll", auth.verify, (req, res) => {
    // values need to enroll, one for the user who will enroll, one for the course that they would like to enroll

    let data = {
        userId: auth.decode(req.headers.authorization).id,
        courseId: req.body.courseId,
    };

    userControllers.enroll(data).then((resultFromController) => {
        res.send(resultFromController);
    });
});

module.exports = router;
