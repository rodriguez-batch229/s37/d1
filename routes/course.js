const express = require("express");
const router = express.Router();
const auth = require("../auth");

const courseController = require("../controllers/courseControllers");

// Create course

router.post("/", (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if (userData.isAdmin == true) {
        courseController.addCourse(req.body).then((resultFromController) => {
            res.send(resultFromController);
        });
    } else {
        res.send("You are not allowed to add a new course");
    }
});

router.get("/AllCourses", (req, res) => {
    courseController.getAllCourses(req.body).then((resultFromController) => {
        res.send(resultFromController);
    });
});

router.get("/getAllActiveCourses", (req, res) => {
    courseController
        .getAllActiveCourses(req.body)
        .then((resultFromController) => {
            res.send(resultFromController);
        });
});

// retrieve a specific course
router.get("/:courseId", (req, res) => {
    courseController.getCourse(req.params).then((resulFromController) => {
        res.send(resulFromController);
    });
});

// Update a course

router.put("/:courseId", auth.verify, (req, res) => {
    courseController
        .updateCourse(req.params, req.body)
        .then((resultFromController) => {
            res.send(resultFromController);
        });
});

router.put("/archive/:courseId", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if (userData.isAdmin == true) {
        courseController
            .archiveCourse(req.params, req.body)
            .then((resultFromController) => {
                res.send(resultFromController);
            });
    } else {
        res.send("Archived Unsucessful");
    }
});

// export the router object for index.js file
module.exports = router;
